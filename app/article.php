<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class article extends Model
{
    public function author(){
        return $this->belongsTo('App\Author', 'id_author', 'id');
    }
}
