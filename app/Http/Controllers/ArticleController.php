<?php

namespace App\Http\Controllers;

use App\article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(Auth::user());
        $data['articles'] = article::all();
        // dd($article);
        $data['title'] = "This Is Sparta";
        return view('articles.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('articles.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $newArticle = new Article();
        $newArticle->judul=$request->judul;
        $newArticle->konten=$request->konten;
        $newArticle->tanggal=$request->tanggal;
        $newArticle->id_author=$request->id_author;
        $file       = $request->file('file');
        $fileName   = $file->getClientOriginalName();
        $request->file('file')->move("image/", $fileName);

        $newArticle->file = $fileName;
        $newArticle->save();

        return redirect()->route('artikel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\article  $article
     * @return \Illuminate\Http\Response
     */
    public function show($article_id)
    {
        $data['article'] = article::find($article_id);
        return view('articles.home', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit($article_id)
    {
        $data['article'] = article::find($article_id);
        return view('articles.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $article_id)
    {
        $article= article::find($article_id);
        $article->judul = $request->judul;
        $article->konten = $request->konten;
        $article->tanggal = $request->tanggal;
        $article->id_author=$request->id_author;
        if($request->file('file') == "")
        {
            $article->file = $article->file;
        } 
        else
        {
            $file       = $request->file('file');
            $fileName   = $file->getClientOriginalName();
            $request->file('file')->move("image/", $fileName);

            $article->file = $fileName;
        }
        $article->save();
        return redirect()->route("artikel.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy($article_id)
    {
        // dd($article_id);
        $article= article::find($article_id);
        $article->delete();
        return redirect()->route("artikel.index");
    }
}
