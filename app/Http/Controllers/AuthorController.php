<?php

namespace App\Http\Controllers;

use App\Author;
use Illuminate\Http\Request;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['authors'] = Author::all();
        return view('authors.tampilauthor', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('authors.inputauthor');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newAuthor = new Author();
        $newAuthor->nama=$request->nama;
        $newAuthor->email=$request->email;
        $newAuthor->password=$request->password;
        $newAuthor->no_hp=$request->no_hp;
        $newAuthor->alamat=$request->alamat;
        $newAuthor->perusahaan=$request->perusahaan;
        $newAuthor->alamat_perusahaan=$request->alamat_perusahaan;
        $newAuthor->save();

        return redirect()->route('author.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit($author_id)
    {
        $data['author'] = Author::find($author_id);
        return view('authors.inputauthor', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $author_id)
    {
        $author= Author::find($author_id);
        $author->nama = $request->nama;
        $author->email = $request->email;
        $author->password = $request->password;
        $author->no_hp = $request->no_hp;
        $author->alamat = $request->alamat;
        $author->perusahaan = $request->perusahaan;
        $author->alamat_perusahaan = $request->alamat_perusahaan;
        $author->save();
        return redirect()->route("author.index");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy($author_id)
    {
        $author= Author::find($author_id);
        $author->delete();
        return redirect()->route("author.index");
    }
}
