@extends('layouts');
@section('content')

<table class="table">
@foreach ($articles as $article)
        <div class="card" style="width: auto;">
        <div class="card-body" style="width: auto;">
        <h5 class="card-title">{{$article->judul}}</h5>
        <img class="card" src="../public/image/{{$article->file}}" style="height:300px">
        <h6 class="card-title">{{$article->tanggal}}</h6>
        <p class="card-text">{{substr($article->konten, 0, 400)}} {{strlen($article->konten)>10? '...':''}}</p>
        <a href="{{route('artikel.show', ['artikel'=>$article->id])}}" class="btn btn-primary">See More</a>
        <a href="{{route('artikel.edit', ['artikel'=>$article->id])}}" class="btn btn-primary">Edit</a>
        <form action="{{route('artikel.destroy', ['artikel'=>$article->id])}}" method="post">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-primary">Delete</button>
        </form>
    </div>
    </div>
@endforeach
    </thead>
</table>
<a href="{{route('artikel.create')}}" class="btn btn-success">Create New Articles</a>
@endsection