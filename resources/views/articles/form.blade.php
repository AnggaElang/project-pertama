@extends('layouts');
@section('content')

    @if(@!$article)
    <form method="POST" action="{{route('artikel.store')}}" enctype="multipart/form-data">
    @method('POST')
    @else
    <form method="POST" action="{{route('artikel.update', ['articles'=>$article->id])}}"enctype="multipart/form-data">
    @method('PUT')
    @endif

    @csrf
    <div class="form-group">
        <label for="exampleInputEmail1">Judul Artikel</label>
        <input type="text" class="form-control" id="name"  placeholder="Enter Judul" name="judul" value="{{@$article->judul}}">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Artikel</label>
        <textarea name="konten" class="form-control" id="konten" placeholder="Masukan Artikel" value="{{@$article->konten}}" cols="30" rows="10">{{@$article->konten}}</textarea>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Tanggal Publish</label>
        <input type="date" class="form-control" id="password" placeholder="Masukan Tanggal Unggah" name="tanggal" value="{{@$article->tanggal}}">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Author</label>
        <input type="text" class="form-control" id="id_author" placeholder="Masukan id author" name="id_author" value="{{@$article->id_author}}">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Masukan gambar</label>
        <input type="file" class="form-control" id="file" placeholder="Masukan Gambar" name="file" value="{{@$article->file}}">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
