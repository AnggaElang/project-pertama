@extends('layouts')
@section('content')
    <div class="card" style="width: auto;">
    <div class="card-body" style="width: auto;">
        <h5 class="card-title">{{$article->judul}}</h5>
        <img class="card" src="{{asset('../public/image/'.$article->file)}}" style="height:300px; margin:center">
        <p class="card-text">{{$article->konten}}</p>
        <p><h6 style="text-align:right">Ditulis Oleh {{ $article->author->nama }}<h6></p>
        <a href="{{route('artikel.index')}}" class="btn btn-primary">Kembali</a>
    </div>
    </div>
@endsection