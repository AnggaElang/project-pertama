@extends('layouts');
@section('content')

<table class="table">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
            <th>E-mail</th>
            <th>Nomor HP</th>
            <th>Alamat</th>
            <th>Perusahaan</th>
            <th>Alamat Perusahaan</th>
        </tr>
@foreach ($authors as $author)
        <tr>
            <th>{{$author->id}}</th>
            <th>{{$author->nama}}</th>
            <th>{{$author->email}}</th>
            <th>{{$author->no_hp}}</th>
            <th>{{$author->alamat}}</th>
            <th>{{$author->perusahaan}}</th>
            <th>{{$author->alamat_perusahaan}}</th>
            <th><a href="{{route('author.edit', ['author'=>$author->id])}}">Edit</a>
                <form action="{{route('author.destroy', ['author'=>$author->id])}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button type="submit">Delete</button>
                </form>
            </th>
        </tr>
@endforeach
    </thead>
</table>
<a href="{{route('author.create')}}">Create New Author</a>

@endsection