@extends('layouts');
@section('content')

@if(@!$author)
<form method="POST" action="{{route('author.store')}}">
@method('POST')
@else
<form method="POST" action="{{route('author.update', ['authors'=>$author->id])}}">
@method('PUT')
@endif
@csrf
  <div class="form-group">
    <label for="exampleInputEmail1">Nama</label>
    <input type="text" class="form-control" id="nama" placeholder="Enter Nama" name="nama" value="{{@$author->nama}}">
  </div> 
  <div class="form-group">
    <label for="exampleInputEmail1">Email address</label>
    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="{{@$author->email}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password</label>
    <input type="password" class="form-control" id="password" placeholder="Password" name="password" value="{{@$author->password}}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Nomor Telepon/HP</label>
    <input type="text" class="form-control" id="no_hp" placeholder="Enter Nomor" name="no_hp" value="{{@$author->no_hp}}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Alamat</label>
    <input type="text" class="form-control" id="alamat" placeholder="Enter Alamat" name="alamat" value="{{@$author->alamat}}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Perusahaan</label>
    <input type="text" class="form-control" id="perusahaan" placeholder="Masukan Perusahaan Anda" name="perusahaan" value="{{@$author->perusahaan}}">
  </div>
  <div class="form-group">
    <label for="exampleInputEmail1">Alamat Perusahaan</label>
    <input type="text" class="form-control" id="alamat_perusahaan" placeholder="Enter Alamat Perusahaan" name="alamat_perusahaan" value="{{@$author->alamat_perusahaan}}">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection