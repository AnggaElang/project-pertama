<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siengaja', function(){
    return "iseng aja";
});

// Route::get('/artikel', 'ArticleController@index')->name('article.index');

Route::resource('/artikel', 'ArticleController')->middleware('auth');

Route::resource('/author', 'AuthorController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
